const axios = require('axios')

// Docs on event and context https://docs.netlify.com/functions/build/#code-your-function-2
const handler = async () => {
    try {
        const response = await axios.get(' https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest', {
            headers: {
                'X-CMC_PRO_API_KEY': process.env.COINMARKETCAP_KEY,
            }
        })

        return {
            statusCode: 200,
            body: JSON.stringify(response.data),
            headers: {
                'content-type': 'application/json'
            },
        }
    } catch (error) {
        return {statusCode: 500, body: error.toString()}
    }
}

module.exports = {handler}
