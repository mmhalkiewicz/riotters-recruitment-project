import {createApp} from "vue";
import App from "./App.vue";
import "@/assets/styles/app.scss";
import VueApexCharts from "vue3-apexcharts";
import Cryptoicon from 'vue-cryptoicon/src/components/Cryptoicon'
import icon from "vue-cryptoicon/src/icons";

Cryptoicon.add(icon);

const app = createApp(App);

app.use(VueApexCharts)

createApp(App).mount("#app");
